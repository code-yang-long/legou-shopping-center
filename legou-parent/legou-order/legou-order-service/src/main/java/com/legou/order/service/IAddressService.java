package com.legou.order.service;

import com.lxs.legou.core.service.ICrudService;
import com.lxs.legou.order.po.Address;

public interface IAddressService extends ICrudService<Address> {
}
