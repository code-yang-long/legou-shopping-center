package com.legou.order.service;

import com.lxs.legou.core.service.ICrudService;
import com.lxs.legou.order.po.Order;

public interface IOrderService extends ICrudService<Order> {
    /**
     * 添加订单
     */
    public void add(Order order);


    public void updatePayStatus(String outTradeNo,String tradeNo);
}
