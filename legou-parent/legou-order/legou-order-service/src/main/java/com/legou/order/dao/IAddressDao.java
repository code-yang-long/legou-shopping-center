package com.legou.order.dao;

import com.lxs.legou.core.dao.ICrudDao;
import com.lxs.legou.order.po.Address;

public interface IAddressDao extends ICrudDao<Address> {
}
