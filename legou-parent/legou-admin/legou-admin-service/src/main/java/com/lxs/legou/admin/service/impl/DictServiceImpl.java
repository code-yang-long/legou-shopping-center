package com.lxs.legou.admin.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lxs.legou.admin.po.Dict;
import com.lxs.legou.admin.service.IDictService;
import com.lxs.legou.core.service.impl.CrudServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DictServiceImpl extends CrudServiceImpl<Dict> implements IDictService {

}
