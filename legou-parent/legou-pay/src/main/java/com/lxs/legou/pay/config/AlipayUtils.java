package com.lxs.legou.pay.config;

import java.io.FileWriter;
import java.io.IOException;

public class AlipayUtils {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "2021000118654740";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCBY0qBp9q+mxWFS81Fv9vpeL7m9qzBbOcKIJ0fv2cZ2nIz0SLvJH9MNMhBmlK3Cnn2aPosX7zaNKKhUL+VXkS+cFV5NG77tdPh5dMMqxOc8Z+Np/pV7jm9X4j7UpIYxu1VkB7s1fQeL4jT+LZOTRL1AIfjrMvZo7Js0qCUGE451MC249SPLN5F7nZltLQeAyMxAfQ7FH9cquIY+TrTl186clfGCpwIA8D/RfYGbqbXGzK4GbkezL2M84vDBKY9NzeCb0Y0GeI0k03zPqFkxGdMotTgeD2gUedqbd2X4QzoqyEBRlJjam6cAnH7ez9DLLp8FsZ0Kd4mwaFNe1LWTJIfAgMBAAECggEAKVzrma1mgZ6kb5t/APhHvkiKht1+9srwxU0NzfNra1lpPuVxzI+XXm5VQpuqK7feq4Z9y6fDgOJoxzdAN+kOAu94c6nufOtw0/ubz6X1PCwGPMvhfI0Oylp7ORT5fZBCmSLcsAvp4aiCjnkpFrkPAWHtDzzTYj0xshZXnWBzyYqi5WW62rlmnzzUeBLnpt55O8nnJlTnTSCMvWeMfiBaXkYr7WNJPPXY2QhV+g1uFFq9ZPyZG4SpqdJKWNTGyDiIgGvSOjjV29fGwNC/zUJYIXSnF70kwsaFpcDFnGJSBpW3U/zF+inAE6XRqyArvnu/+og+nR3av14UXgcx1XfNyQKBgQDZgHzGof0da2ImHhysTrVSijuxuldOtfQiR6vlxzYV3RUJfiNM6MzWB/gasqS0CqtBEc7x2wf/6XGDSYst6l1sSqL/eyoP17i3+YIMzUQN29SW20VbiCJMAYFjY7Mwi15TnIMBGRGJa0e8eE7F4lIsyQnexIjv8JWO0QGfwIFg7QKBgQCYSicrozWs4Uy3pCkMfg7ydUxQpkyuncu7HSRNV5b7pO/L+IOhkpjtvcAdyzl4MiZ8iK+Cs9spD+c0SlHlvqSPICNXQRFZgjY1cDSvf3b/QTcfk/mIiuHT+vwA1KoGb4JrmLFCIJ9rm0e5eNajKMZWcRPOlXZJtGV68e39RU45uwKBgQDZfRbIFOEikudREFlBn27G+30XiiqVhXJ6+MoV+W7GzAymMmCgHkX9W3inUa1Kb0Sxy1nxFEFakBc23cpzfmAu0i9NYt47e021gaSeon8DkKdytg4TqfwJnpPAS8i5qRMIbn+9EFEt5X2VI4Q5cccUC294N3Rq4VrbtXl3QirdPQKBgDI+VMoK4MyW0G7OFFTwyhGoLXN2SlpZ6Ttkk/OIcGG5uvu6lAMckKPvNJEf2WgUc6omlnEy0Z2sh4Uy9+4qhbRK0B4bc5PqZ+QdjuRP03zHkV4zveIUoAkGB1pmz2msJIVHLUQ56x7fbjx856OQ8k7Wo8hGe245vDlh1Dj5QqO9AoGBAJUxKtBdodyB2f3Xbh4rv90TnkXfWSEJMkgwVvKRBHwse3w/4JllFB9sSDnFbSLdBDj0IEGxAYuaGgbyGc7o54qIZdj5rgtT/vweEgMWpELJHAkWxRtxB/ERGY4kcVxOXXJRrgwebRrOiUHK5+2Ss4CJg/Y1lBhV++6uFaPURTil";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3Q93v6zVkbKuKoGbda+O2hWheBLnvgwXMCWSkeyb+l01wZKwFdDXNCpbmG2uKSxQPz6+CVVnFzrIXERNvHXs6rERort5hzOLcSfDpDXG/IPhh+Z0V9Ik/Yb7tM0TQDiRQh+lJwEC/6SqBoxCy67sXpy8CrZudljIlaDXfySDH2a40wSP+lqauY2rfuGSSfArxHhZS3/blfzM4HC6kURI5E26kYLQAPeiGfgeh88GeT3lJppRtnrpDy6QKxsLRJJL/52D5gYXxurvx1DESWlS9PcFHv+5aZ4QdK+/ja45A26g7tn6ah6hVd9GUoAVCr9H9rfEMUsq02ZegDenIpeY+wIDAQAB";

    // 服务器异步通知页面路径
    //需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://mnt5dx.natappfree.cc/getnotify";

    // 页面跳转同步通知页面路径
    //需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://mnt5dx.natappfree.cc/getreturn";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关,注意这些使用的是沙箱的支付宝网关，与正常网关的区别是多了dev
    public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    // 支付宝网关
    public static String log_path = "C:\\";


    //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
