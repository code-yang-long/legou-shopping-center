package com.service.auth.serviceauth;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.test.context.junit4.SpringRunner;


//@RunWith(SpringRunner.class)
//@SpringBootTest
public class JWTTest {

    @Test
    public void testGenPassword() {
//        System.out.println(new BCryptPasswordEncoder().encode("abcdefg"));
        System.out.println(new BCryptPasswordEncoder().matches("1234562", "$2a$10$cqDiBXGunn/GcEeKvZX62eABaQyjyf3eJEW7AIL98uiq8UM.VZZfi"));
    }

    //生成一个jwt令牌
    @Test
    public void testCreateJwt() throws Exception {
        //证书文件
        String key_location = "mickey.jks";
        //密钥库密码
        String keystore_password = "kaikeba";
        //访问证书路径
        ClassPathResource resource = new ClassPathResource(key_location);
        //密钥工厂
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(resource, keystore_password.toCharArray());
        //密钥的密码，此密码和别名要匹配
        String keypassword = "kaikeba";
        //密钥别名
        String alias = "mickey";
        //密钥对（密钥和公钥）
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(alias, keypassword.toCharArray());
        //私钥
        RSAPrivateKey aPrivate = (RSAPrivateKey) keyPair.getPrivate();
        //定义payload信息
        Map<String, Object> tokenMap = new HashMap<String, Object>();
        tokenMap.put("id", "123");
        tokenMap.put("name", "mrt");
        tokenMap.put("roles", "r01,r02");
        tokenMap.put("ext", "1");
        //生成jwt令牌
        Jwt jwt = JwtHelper.encode(new ObjectMapper().writeValueAsString(tokenMap), new RsaSigner(aPrivate));
        //取出jwt令牌
        String token = jwt.getEncoded();
        System.out.println("token=" + token);
    }

    //资源服务使用公钥验证jwt的合法性，并对jwt解码
    @Test
    public void testVerify() {
        //jwt令牌
        String token
                = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHQiOiIxIiwicm9sZXMiOiJyMDEscjAyIiwibmFtZSI6Im1ydCIsImlkIjoiMTIzIn0.a9ZNVejkS-qkigSXW1rGgLAG4Y8uT9nZ2CS1cWE4mzyPsC0g1fc5GeKw8XKqCwehj8K6lDDBOk_vfmo_pkF7LNgczSadFtpyUCnj4iwgdpf5ZCa9Lohc_D-IzragZSwxkItiFuAU5KbC3jVxM0jd0wMM_ru2l661BbiukFgcRkcN6y08R_DQndYMbktOq-MdOXmksqkyaEDMqzx7QXQ8Uwl7sRv7CxDtWvKKqHgxtRqDvW8WRxNeZj8BDgCGRW5L4ZRFnkLt7b6xe3d6npEVp6w4fE1Q0pHsfDII629Nz08DYk7eI831IHoaBataiInUxrI_U4XAlDky6Ksu41xLzg";
        //公钥
        String publickey = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmh61dL2jU393calXlcw2Q40I23ka2IljseJDOGWZRO2pk6uUKbOmtS3MHcMr0GEj7OUy7FJVT+USoo4vero1C+yw155q6zfChcantf2bQUX6IU69KvBadzUdB0hu2JggK+1ryRrCPF13dh99uDqlQOEV4T8JLDJHJh2iCGWLMg/wgpKI0PkTcudLLuowsA+PoPjbUMZmS/ROkhBHlPrm7r+XHZlAqBgTgEazTjV4NJb3fJ79R+7zx+XpR1Melr6iBiq0r6VbwZ+ArsADt45DH16H8AK3iVthyUX1CdKuZbTNKHzVNhLIPGe7w5NwqcePzy7LfCARp+G+thVYl7djawIDAQAB-----END PUBLIC KEY-----";
        //校验jwt
        Jwt jwt = JwtHelper.decodeAndVerify(token, new RsaVerifier(publickey));
        //获取jwt原始内容
        String claims = jwt.getClaims();
        System.out.println(claims);
        //jwt令牌
        String encoded = jwt.getEncoded();
        System.out.println(encoded);
    }


}